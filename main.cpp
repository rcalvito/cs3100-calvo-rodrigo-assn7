#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include <queue>
#include <iterator>
#include <chrono>
#include <memory>
#include <thread>
#include <mutex>

const int SEQUENCE_SIZE = 1000;
const unsigned int PAGE_MIN = 1;
const unsigned int PAGE_MAX = 250;
std::mutex myMutex;


void threadWorker(int* sequenceNo, int* anomalies){

	std::random_device rd;
	std::mt19937 engine{rd()};
	std::uniform_int_distribution<unsigned int> dist{PAGE_MIN, PAGE_MAX};
	std::vector<unsigned int> sequence;
	sequence.resize(SEQUENCE_SIZE);
	std::generate(sequence.begin(), sequence.end(), [&](){ return dist(engine); });
	std::vector<unsigned int> result;
	int pageFaults=0;
	int faultsBefore=0;
	while((*sequenceNo)<(int)(100-std::thread::hardware_concurrency())){
		sequence.clear();	
		sequence.resize(SEQUENCE_SIZE);
		std::generate(sequence.begin(), sequence.end(), [&](){ return dist(engine); });
		for(int i=1;i<=100;i++){
			result.clear();
			result.resize(i);
			for(int j=0;j<1000 ;j++){
				if(std::find(std::begin(result),std::end(result),sequence[j])!=std::end(result)){
					
				}
				else if((int)result.size()<i){
					pageFaults++;
					result.push_back(sequence[j]);
				}
				else{
					pageFaults++;
					result.erase(result.begin());
					result.push_back(sequence[j]);
			
				}
			}
			if(faultsBefore<pageFaults && faultsBefore!=0){
				
				myMutex.lock();
				(*anomalies)++;
				std::cout<<"anomaly found"<<std::endl;
				std::cout<<"Sequence: "<<(*sequenceNo)+1<< std::endl;
				std::cout<<"Page faults: "<< faultsBefore<< " @ Frame size: "<<i-1<<std::endl;
				std::cout<<"Page faults: "<< pageFaults<< " @ Frame size: "<<i<<std::endl<<std::endl;
				myMutex.unlock();
			}
			faultsBefore=pageFaults;
			pageFaults=0;
		}
		myMutex.lock();
		(*sequenceNo)++;
		myMutex.unlock();
		faultsBefore=0;
		pageFaults=0;
	}

}

int main(void){
	std::cout<<"Belady's anomaly"<<std::endl;
	std::cout<<"Lemgth of memory reference string: "<<SEQUENCE_SIZE<<std::endl;
	std::cout<<"Frames of physical memory: 100"<<std::endl<<std::endl;
	auto before = std::chrono::high_resolution_clock::now();
	int* sequenceNo= new int; 
	int *anomalies = new int;
	(*sequenceNo) = 0; 
	(*anomalies) = 0;
	std::vector<std::shared_ptr<std::thread>> threads;
	for(std::uint16_t core = 0;core<std::thread::hardware_concurrency(); core++){
		threads.push_back(std::make_shared<std::thread>(threadWorker,sequenceNo, anomalies));
		//(*sequenceNo)++;
		//std::cout<<(*sequenceNo)<<std::endl;
	}
	for(auto&& thread : threads){
		thread->join(); 
	}

	std::cout<<"Anomalies found: "<< *anomalies<<std::endl<<std::endl;
	delete  sequenceNo;
	delete anomalies;
	auto after = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> dur = after-before;	
	std::cout<<"It took: "<< dur.count()<<" seconds to run the program"<<std::endl<<std::endl;

	
}







